# Copyright 2012 Marc-Antoine Perennou<Marc-Antoine@Perennou.com>
# Copyright 2012-2015 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require bash-completion python autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.12 1.11 ] ]

export_exlib_phases src_prepare src_install

SUMMARY="tools for accessing and modifying virtual machine disk images"
HOMEPAGE="http://${PN}.org/"
if (( $(ever range 2) % 2 )); then
    version_path_suffix="development"
else
    version_path_suffix="stable"
fi
DOWNLOADS="
    ${HOMEPAGE}/download/$(ever range 1-2)-${version_path_suffix}/${PNV}.tar.gz
    ${HOMEPAGE}/download/binaries/appliance/appliance-1.28.1.tar.xz
"

BUGS_TO=""

LICENCES="GPL-2 LGPL-2"
SLOT="0"
MYOPTIONS="
    fuse [[ description = [ Allow guestmount to mount VM images ] ]]
    gobject-introspection
    java [[ description = [ Enable Java language bindings ] ]]
    ocaml [[ description = [ Enable OCaml language bindings ] ]]
    perl [[ description = [ Enable Perl language bindings ] ]]
    php [[ description = [ Enable PHP language bindings ] ]]
    python [[ description = [ Enable Python language bindings ] ]]
    ruby [[ description = [ Enable Ruby language bindings ] ]]
"

# No tests for libguestfs itself but just for gnulib. And some of them fail due
# to sandboxing.
RESTRICT="test"

DEPENDENCIES="
    build:
        sys-devel/gettext
        ocaml? ( dev-lang/ocaml )
    build+run:
        app-cdr/genisoimage [[ note = [ configure fails if genisoimage is not installed ] ]]
        app-virtualization/qemu
        dev-libs/glib:2[>=2.26]
        dev-libs/gnutls
        dev-libs/libconfig
        net-libs/libnl:3.0
        dev-libs/libxml2:2.0
        dev-libs/pcre
        dev-util/gperf
        sys-apps/file
        sys-fs/lvm2
        virtualization-lib/libvirt[>=0.10.2]
        fuse? ( sys-fs/fuse:0 )
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.30] )
        java? ( virtual/jdk )
        perl? ( dev-lang/perl:= )
        php? ( dev-lang/php )
        python? ( dev-lang/python:2.7 )
        ruby? ( dev-lang/ruby )
"

if ever at_least 1.23.0; then
    DEPENDENCIES+="
        build+run:
            app-admin/augeas[>=1.0.0]
            sys-apps/systemd[>=196]
    "
fi

if ever at_least 1.29.0; then
    DEPENDENCIES+="
        build+run:
            app-arch/unzip
            app-arch/zip
    "
fi

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-gobject
    --enable-threads
    --disable-appliance
    --disable-daemon
    --disable-erlang
    --disable-golang
    --disable-haskell
    --disable-werror
    --with-default-backend=libvirt
    --with-extra="-Exherbo"
    --with-readline
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    fuse
    "gobject-introspection introspection"
    ocaml
    perl
    php
    python
    ruby
)

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=( "java java /usr/${LIBDIR}/jdk" )

AT_M4DIR=( m4 )

libguestfs_src_prepare() {
    # Disable non-smart version check for qemu
    edo sed -i -e "s:\(version \)@.*:\1'; then:" configure.ac

    autotools_src_prepare
}

libguestfs_src_install() {
    default

    if ! ever at_least 1.26.1 ; then
        if option bash-completion ; then
            dobashcompletion "${IMAGE}"/etc/bash_completion.d/guestfish-bash-completion.sh
        fi
        edo rm -r "${IMAGE}"/etc/bash_completion.d
    fi

    option python && python_bytecompile

    insinto /usr/${LIBDIR}/guestfs/
    doins ../appliance/*
}

