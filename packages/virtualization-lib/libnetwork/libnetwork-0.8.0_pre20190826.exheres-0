# Copyright 2017 Arnaud Lefebvre <arnaud.lefebvre@clever-cloud.com>

# This package provides the docker-proxy binary

require github [ user="docker" rev="92d1fbe1eb0883cf11d283cea8e658275146411d" ]

SUMMARY="Docker Networking"
DESCRIPTION="
Libnetwork provides a native Go implementation for connecting containers
The goal of libnetwork is to deliver a robust Container Network Model that
provides a consistent programming interface and the required network
abstractions for applications.
"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-lang/go[>=1.12.8]
    build+run:
        !app-virtualization/docker[<1.13] [[
            description = [ conflicting docker-proxy binary ]
            resolution = [ upgrade-blocked-before ]
        ]]
"

BUGS_TO="arnaud.lefebvre@clever-cloud.com"

RESTRICT="test strip"

src_prepare() {
    # symbolic links for deps doesn't work
    edo mkdir -p "${WORKBASE}"/go-deps/src
    edo mv "${WORK}"/vendor/* "${WORKBASE}"/go-deps/src
    edo ln -s "${WORK}" "${WORKBASE}"/go-deps/src/github.com/docker/${PN}
}

src_compile() {
    export GOPATH="${WORKBASE}"/go-deps
    edo cd ${GOPATH}/src/github.com/docker/${PN}
    edo make build-local
}

src_install() {
    dobin bin/dnet
    dobin bin/docker-proxy
    edo cd docs
    edo rm -r vagrant-systemd
    dodoc -r ./*
}
